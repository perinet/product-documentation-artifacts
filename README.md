# Public Product Documentation Artifacts Repository

Storage for permalinks of documentation artifacts for Perinet products.

```
https://docs.perinet.io/<URLenc(path)>
```

example: `<URLenc(path)>` -> `PRN100546 - periCORE Development Kit Product Summary.pdf`

```
https://docs.perinet.io/PRN100546%20-%20periCORE%20Development%20Kit%20Product%20Summary.pdf for PRN100546 - periCORE Development Kit Product Summary.pdf
```
